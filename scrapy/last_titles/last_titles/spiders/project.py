import scrapy

class LastTSpider(scrapy.Spider):
    name = "last_titles"
    def __init__(self, site, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_urls = [site]
        self.DOMAIN = site.split("//")[1]

    def parse(self, response):
        for quote in response.css("div.caption h4"):
            yield {"title" : quote.css("a::text").extract_first(),
                    "url" : quote.css("a::attr(href)").extract_first()
                    }
