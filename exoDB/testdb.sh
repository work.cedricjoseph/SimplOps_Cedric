#!/bin/sh
# -*- coding: utf-8 -*-

#fonction ecriture et maj
write_data() {
    #check_entry $1
    #on check si l'entrée correspond a quelque chose du genre "entree=data"
    if (( echo $1 | grep -q '=' ))
        then
            #si oui, on lance fonction creation de fichier (existe ou non ?)
            create_file $2
            #key se trouve a gauche du signe =
            key=$(echo $1 | cut -d '=' -f1)
            #echo "$key"

            #value se trouve a droite du signe =
            value=$(echo $1 | cut -d '=' -f2-)
            #echo "$value"

            #on check si la key existe ou pas
            if ((grep -q $key $2))
                then
                    #si oui...
                    #la partie update
                    #on repere le numero de ligne de la key
                    num_line=$(grep -n ^$key= $2 | cut -d: -f1)
                    #echo $num_line

                    #on récupère la valeur de la key déjà enregistrée
                    old_value=$(grep ^$key= $2 | cut -d '=' -f2-)
                    #echo $old_value

                    #on remplace l'ancienne valeur par la nouvelle
                    sed -i "$num_line s/$old_value/$value/g" $2
                    echo "value updated"
                else
                    #sinon, on écrit la valeur dans le fichier
                    echo $1 >> $2
                    echo "written !"
            fi    
        else

            #si l'entrée ne correspond pas à "entree=data"...
            echo "entrée invalide"
    fi
}

#fonction lecture
read_data() {
    #on verifie si le fichier existe...(si oui, on continue, si non, exit)
    check_file $2

    #on vérifie si la lecture de la data existe
    if (( grep -E ^$1= $2 ))
                then
                    #si oui, ok
                    true

                else
                    #si non...
                    echo "not found"
    fi
}

#fonction lecture par valeur
read_value() {
    #on verifie si le fichier existe...(si oui, on continue, si non, exit)
    check_file $2

    #on vérifie si la lecture de la value existe
    if (( grep -Eq =$1$ $2 ))
        then
            true
            echo $1 | cut -d '=' -f1
        else
            echo "not found"
    fi
}

read_key() {
   check_file $2
   if (( grep -Eq ^$1= $2 ))
               then
                   true
                   echo $1 | cut -d '=' -f2-              
               else
                   echo "not found"
   fi
}

#fonction efface
delete_data() {
    #on verifie si le fichier existe...(si oui, on continue, si non, exit)
    check_file $2

    #on vérifie si la data existe
    if (( grep -q -E ^$1= $2 ))
                then
                    #si oui, on efface cette entrée...
                    sed -i "/^$1=/d" $2
                    echo "$1 has been deleted"
                else
                    echo "not found"
    fi
}

create_file() {
    #le fichier existe ?
    if [ -e $1 ]
        then
            #oui, on continue
            true
        else
            #non, alors creer un nouveau fichier ?
            read -p "file doesn't exist, do you want to create it? yes/no: " response
            case "$response" in
                yes)
                    #oui alors créer
                    touch $1
                ;;
                no)
                    #non, alors merci aurevoir
                    echo "ok, good bye"
                    exit
                ;;
                *)
                    #autre ? pas compris, on repose la question...
                    echo "say what ?"
                    create_file $1
                ;;
            esac
    fi
}

check_file() {
    #le fichier demandé existe ?
    if [ -e $1 ]
        then
            #oui, on continue
            true
        else
            #non, on ne peut pas travailler
            echo "data base doesn't exist"
            exit
    fi
}

check_entry() {

    regex="^.+=.+"
    echo $1
    #regex="^([a-zA-Z0-9_\-\.\+]+)=([a-zA-Z0-9_\-\.\+]+)$"
    if ( $1 =~ $regex );
    then 
        true
        echo "OK"
    else 
        echo "bad entry"
        
   fi
}


#MODE INTERACTIF
if [ $# -eq 1 ]
        	then
            read -p "What should I do : " instruction
            #option a gauche de l'entrée instruction
            option=$(echo $instruction | cut -d ' ' -f1)
            #data à droite de l'entrée instruction
            data=$(echo $instruction | cut -d ' ' -f2-)

            case "$option" in
                read)
					read_data $data $1
                ;;
                write)
                    write_data $data $1
                ;;
                delete)	
					delete_data $data $1
                ;;
                value)
                    read_value $data $1
                ;;
                key)
                    read_key $data $1
                ;;
                *)
                    echo "unknown instruction"
                ;;
            esac
fi

#choix des options
while getopts r:d:w:v:k option
do 
    if  [ $# -eq 3 ]
        then
            case "${option}" in
                r)
					read_data ${OPTARG} $3
                ;;

                w)
                    write_data ${OPTARG} $3
                ;;
                d)	
					delete_data ${OPTARG} $3
                ;;
                v)
                    read_value ${OPTARG} $3
                ;;

                k)  read_key ${OPTARG} $3
                ;;

                *)
                    echo "mauvais arg"
                    ;;
            esac
        else
            echo "pas bon"
            echo "testbababa"
    fi
done