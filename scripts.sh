#!/bin/bash

echo Date : $(date +'%d/%m/%Y %H:%M:%S')
echo user : $USER
echo PAth : $(pwd)
echo Hostname : $(hostname)
echo Kernel : $(uname -mr)
echo Processes : $(ps -edf --no-headers | wc -l)
echo Disk left : $(df -h / | awk 'END {print $4}')
echo Ram left : $(free -h | awk 'NR==2 {print $7}')
echo LastUser : $(cat /etc/passwd | awk 'END {print $1}' | cut -d : -f 1)
echo Biggest File : $(du ~ -hax | sort -rh | sed -n "2 p")





